#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Simple Telegram bot that sends Linus Torvalds's quotes
# Copyright 2017 SlavMetal <7tc@protonmail.com>
#
# Much like the program fortune but with quotes from Linus Torvalds
# Copyright 2017 Axel Olivecrona <axel.olivecrona1995@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import redis
import logging
import random
import yaml
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, Filters, CallbackQueryHandler
from functools import wraps
from mwt import MWT

# Open the config.yml file
with open("config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)

# Connecting to Redis db
db = redis.StrictRedis(host=cfg['host'],
                       port=cfg['port'],
                       db=cfg['db'])

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


def restricted(func):
    @wraps(func)
    def wrapped(bot, update, *args, **kwargs):
        user_id = update.effective_user.id
        # if user_id not in LIST_OF_ADMINS:
        if not Filters.private and update.message.from_user.id not in get_admin_ids(bot, update.message.chat_id):
            print("Unauthorized access denied for {}.".format(user_id))
            return
        return func(bot, update, *args, **kwargs)
    return wrapped


@MWT(timeout=60*60)
def get_admin_ids(bot, chat_id):
    """Returns a list of admin IDs for a given chat. Results are cached for 1 hour."""
    return [admin.user.id for admin in bot.get_chat_administrators(chat_id)]


def quote(bot, update):
    lang = db.get(str(update.message.chat_id))

    if lang == b"RU":
        lines = open("quotes_ru").readlines()
    else:
        lines = open("quotes_en").readlines()

    line = random.choice(lines)[:-1:]
    update.message.reply_text(line, reply_to_message_id=update.message.message_id)


@restricted
def set_lang(bot, update):
    # Create inline keyboard
    keyboard = [[InlineKeyboardButton("English", callback_data='EN'),
                 InlineKeyboardButton("Russian", callback_data='RU')]]
    reply_markup = InlineKeyboardMarkup(keyboard)

    # Send keyboard
    update.message.reply_text('Choose a language:', reply_markup=reply_markup)


@restricted
def pressed_keyboard(bot, update):
    query = update.callback_query
    language = query.data
    chat_id = query.message.chat_id
    # Why it's not working?
    # chat_id = update.message.chat_id

    # Write user's language to Redis
    if language == "EN":
        db.set(str(chat_id), language)
    elif language == "RU":
        db.set(str(chat_id), language)

    bot.edit_message_text(text="Language chosen: %s" % language,
                          chat_id=query.message.chat_id,
                          message_id=query.message.message_id)


def error(bot, update, error):
    logger.warning('Update "%s" caused error "%s"' % (update, error))


def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(cfg['botapi_token'])

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # Command handlers
    dp.add_handler(CommandHandler("quote", quote))
    dp.add_handler(CommandHandler("setlang", set_lang))
    # Inline keyboard handler
    updater.dispatcher.add_handler(CallbackQueryHandler(pressed_keyboard))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT.
    updater.idle()


if __name__ == '__main__':
    main()
